# Valutakalkulator

Dette er en enkel valutakalkulator-nettside som lar deg konvertere mellom ulike valutaer basert på dagens konverteringskurs.

## Bruksanvisning
Åpne nettsiden i en nettleser.
Velg beløpet du ønsker å konvertere ved å skrive det inn i "Beløp"-feltet.
Velg basisvaluta fra den første rullegardinlisten ("Fra valuta").
Velg målvaluta fra den andre rullegardinlisten ("Til valuta").
Klikk på "Konverter"-knappen.

Resultatet av konverteringen vil vises under knappen, og det vil vise hvor mye beløpet du har angitt tilsvarer i målvalutaen.

Du kan også bytte basisvaluta ved å endre valget i "Fra valuta"-rullegardinlisten. Konverteringskursene vil oppdateres automatisk.
## Teknologi og API

### Denne valutakalkulatoren bruker følgende teknologier:

HTML: For strukturering av nettsiden.
CSS: For styling og layout.
JavaScript: For å håndtere interaktivitet og beregne konverteringer.
Google Fonts: For å legge til skrifttypen "Roboto" til nettsiden.
ExchangeRate-API: For å hente dagens konverteringskursdata. API-en støtter over 170 forskjellige valutaer.

## Tilpasning

Du kan tilpasse nettsiden ved å endre stiler i style.css-filen eller legge til flere funksjoner i script.js-filen. Du kan også legge til flere valutaalternativer i rullegardinlistene ved å oppdatere dataene som hentes fra ExchangeRate-API.


## Bidrag

Vi ønsker bidrag fra fellesskapet velkommen. Hvis du har forbedringer eller nye funksjoner å tilføye, kan du opprette en pull-forespørsel til dette prosjektet på GitLab.
