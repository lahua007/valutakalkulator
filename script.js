let out = "";
let out2 = "";
let ut = document.getElementById("out");
let ut2 = document.getElementById("out2");
let input = document.getElementById("tall1");
let input_valuta = document.getElementById("valuta");
let select_valuta_base = document.getElementById("valutaBase");
let base = "USD";
let target;
let result;
let previousResult = ""; // Lagre forrige konvertering
let conversionRates; // Lagre konverteringskursene

function oppdater() {
    // Hent konverteringskursene en gang ved siden av siden lastes
    fetch("https://v6.exchangerate-api.com/v6/494492eade9985833ec7a037/latest/" + base)
        .then((respons) => {
            return respons.json();
        })
        .then((data) => {
            conversionRates = data.conversion_rates;

            // Fyll inn valutaalternativer basert på konverteringskursene
            for (const currency in conversionRates) {
                const option = document.createElement("option");
                option.value = currency;
                option.textContent = currency;
                select_valuta_base.appendChild(option);
            }

            for (const currency in conversionRates) {
                const option2 = document.createElement("option")
                option2.value = currency;
                option2.textContent = currency;
                input_valuta.appendChild(option2);
            }
        });
}

oppdater();

function klikk() {
    oppdater();
    let valutaBaseValue = select_valuta_base.value;
    let valutaValue = input_valuta.value;

    base = valutaBaseValue; // Dynamisk basisvaluta basert på valget fra <select>
    target = valutaValue;
    const inputValue = parseFloat(input.value);

    result = inputValue * conversionRates[target];

    if (isNaN(inputValue)) {
        out = "Skriv inn et gyldig beløp";
    } else {
        if (out == "Skriv inn et gyldig beløp") {
            // Ikke gjør noe hvis forrige melding var ugyldig
        } else {
            out2 = out; // Lagre forrige konvertering i out2
        }
        result = inputValue * conversionRates[target];
        out = inputValue + " " + base + " = " + result + " " + target;
    }
    ut2.innerHTML = out2; // Oppdater ut2 med forrige konvertering
    ut.innerHTML = out;


}





